import java.util.Arrays;

public class QuickSort {

    // quickSort method
    static void run(int[] A, int p, int q){
        if(p < q){
            int r = partition(A, p, q);
            run(A, p, r-1);
            run(A,r+1, q);
        }
    }

    // performs a permutation of A, where
    // elements in [q ... pivotPosition - 1] <= pivot
    // elements in [pivotPosition+1 ... q] > pivot
    // returns the final position of the pivot
    private static int partition(int[] A, int p, int q){
        int x = A[q]; // pivot
        int i = p - 1;
        for(int j = p; j<= q; j++){
            if(A[j] <= x){
                i++;
                swap(A, i, j);
            }
        }
        return i;
    }

    private static void swap(int[] A, int i, int j){
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static void main(String[] args){
        int[] a = {5,8,12,11,3,15,9,10,8,10};
        run(a, 0, a.length-1);
        System.out.println(Arrays.toString(a));

    }
}
