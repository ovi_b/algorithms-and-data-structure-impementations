import java.util.Arrays;

public class InsertionSort {

    public static void run(int[] A){
        for(int j= 1; j< A.length; j++){
            int key = A[j];
            int i = j-1;
            while (i>0 && A[i] > key){
                A[i+1] = A[i];
                i = i-1;
            }
            A[i+1] = key;
        }
    }

    public static void main(String[] args){
        int[] arr = {0, 100, 101, 1, 2, 3, 4, 5};
        run(arr);
        System.out.println(Arrays.toString(arr));
    }
}
