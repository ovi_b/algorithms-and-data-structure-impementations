import java.util.Arrays;

public class MergeSort {

    // Merges two ordered subarrays of A[]
    // first subarray is A[start ... mid]
    // second subarray is A[mid+1 ... end]
    private static void merge(int[] A, int start, int end, int mid){
        int[] B = new int[A.length];
        int i = start;
        int j = mid + 1;
        for (int k = start; k <= end; k++){
            if(i <= mid && j <= end){
                if(A[i] <= A[j]){
                    B[k] = A[i];
                    i++;
                } else {
                    B[k] = A[j];
                    j++;
                }
            } else {
                while(i<=mid){
                    B[k] = A[i];
                    i++;
                    k++;
                }
                while(j<=end){
                    B[k] = A[j];
                    j++;
                    k++;
                }
            }
        }

        // copy merged portion of B in A
        if (end + 1 - start >= 0) System.arraycopy(B, start, A, start, end + 1 - start);
//        for(int k = start; k <= end; k++){
//            A[k] = B[k];
//        }
    }

    public static void run(int[] A, int p, int q){
        if(p < q){
            int r = (p + q) / 2;
            run(A, p, r);
            run(A,r + 1, q);
            merge(A, p, q, r);
        }
    }
    public static void main(String[] args){
        int[] a = { 12, 11, 13, 5, 6, 7};
        run(a, 0, a.length-1);
        System.out.println(Arrays.toString(a));
    }
}
